japa (0.9.4-1) unstable; urgency=medium

  * New upstream version 0.9.4
  * Refresh patch
  * Bump dh-compat to 13
  * Bump Standards-Version to 4.6.0
  * Bump libzita-alsa-pcmi-dev B-D to 0.4.0
  * Change the Architecture of the binary package to linux-any
  * Update d/copyright years
  * Add d/install and clean and minimize d/rules
  * d/japa.manpages: Fix path to manual
  * Bump d/watch version to 4

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 06 Feb 2022 21:51:12 +0100

japa (0.9.2-1) unstable; urgency=medium

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use dh-compat instead of debian/compat
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Olivier Humbert ]
  * d/*.desktop: Add French names and comments
  * d/copyright: Update year, http > https and add myself
  * d/watch: http > https

  [ Dennis Braun ]
  * New upstream release
  * d/control:
    + Bump dh-compat to 12
    + Bump Standards-Version to 4.5.0
    + Add libfreetype6-dev and pkg-config to B-D
    + Set RRR: no
    + Add me as uploader
  * d/copyright: Update year & add myself
  * d/japa.manpages: Fix path for installation
  * d/patches: Refresh patch, add pkgconf fix
  * d/rules: Add prefix and pkgconf export
  * d/source/local-options: Drop, obsolete

 -- Dennis Braun <d_braun@kabelmail.de>  Wed, 05 Feb 2020 20:46:45 +0100

japa (0.8.4-2) unstable; urgency=medium

  * Set dh 10.
  * Update copyright file.
  * Sign tags.
  * Bump Standards.
  * Fix VCS fields.
  * Drop menu file.
  * Fix hardening.
  * Avoid useless linking.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 22 Dec 2016 16:26:22 +0100

japa (0.8.4-1) unstable; urgency=low

  * New upstream release.
  * Fixed URL in watch file.
  * Fixed URL and upstream email.
  * Fixed homepage URL.
  * Set dh/compat 9
  * Tighten build-dep on libclthreads.
  * Tighten build-dep on libclxclient.
  * Bump standards.
  * Fix VCS canonical URLs.
  * Set Priority optional.
  * Don't sign tags.
  * Update copyright file.
  * Added Keywords entry to desktop files.
  * Tune gitignore file to handle rather dir.
  * Update man pages.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 07 Aug 2013 02:19:42 +0200

japa (0.6.0-2) unstable; urgency=low

  * Icon added, update menu and desktop files.
  * Bump Standards.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Fri, 27 May 2011 19:20:07 +0200

japa (0.6.0-1) unstable; urgency=low

  [ Jaromír Mikeš ]
  * New maintainer, ACK'd by Helmut Grohne.
  * Imported Upstream version 0.6.0 (Closes: #576615).
  * Bump compat file up to 7.
  * debian/control: one_dep_per_line style applied.
  * Switch to source format 3.0 (quilt).
  * Switch to debhelper 7 short-form.
  * Remove dpatch support.
  * Vcs-* fields added.
  * Bump standards.
  * Moved Homepage field.
  * Add .gitignore file.
  * Add gbp.conf file.
  * Add local-options file:
    - Abort on upstream changes.
    - Deapply patches after gbp'ing.
  * Convert patches to the quilt format.
  * Refresh patches, add DEP-3 headers.
  * Remove dirs and docs files.
  * Updated copyright file to DEP-5 rev.166.
  * Removed fftw-dev from dependencies (Closes: #494317).

  [ Alessio Treglia ]
  * Add myself to the Uploaders field.

 -- Alessio Treglia <alessio@debian.org>  Thu, 24 Feb 2011 11:28:28 +0100

japa (0.2.1-3) unstable; urgency=low

  * Adapted build dependencies: fftw3-dev -> {lib, | }fftw3-dev.

 -- Helmut Grohne <helmut@subdivi.de>  Tue, 16 Oct 2007 22:38:16 +0200

japa (0.2.1-2) unstable; urgency=low

  * Cleanup debian/rules and debian/watch.
  * Make author singular in copyright file.
  * Added homepage to control.
  * Extended description.
  * Added alsa specific stuff to manpage.
  * Changed "Apps" to "Applications" in menu file.
  * Closes:435583 (ITP)

 -- Helmut Grohne <helmut@subdivi.de>  Thu, 20 Sep 2007 22:41:52 +0200

japa (0.2.1-1) unstable; urgency=low

  * Initial release.

 -- Helmut Grohne <helmut@subdivi.de>  Sat, 07 Jul 2007 20:11:24 +0200

