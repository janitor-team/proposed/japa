// ----------------------------------------------------------------------------
//
//  Copyright (C) 2004-2018 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "styles.h"


struct colors      Colors;
struct fonts       Fonts;
struct xft_colors  XftColors;
struct xft_fonts   XftFonts;

X_button_style  Bst0, BstA, BstB, BstX, BstY;
X_textln_style  Tst0, Tst1;


void init_styles (X_display *disp, X_resman *xrm)
{
    Colors.black = disp->blackpixel ();
    Colors.white = disp->whitepixel ();
    Colors.main_bg   = disp->alloc_color (xrm->get (".color.main.bg",   "#ece8dc"),   Colors.white);
    Colors.main_ds   = disp->alloc_color (xrm->get (".color.main.ds",   "gray20"),    Colors.black);
    Colors.main_ls   = disp->alloc_color (xrm->get (".color.main.ls",   "white"),     Colors.white);
    Colors.spect_bg  = disp->alloc_color (xrm->get (".color.spect.bg",  "white"),     Colors.white);
    Colors.spect_gr  = disp->alloc_color (xrm->get (".color.spect.gr",  "gray80"),    Colors.white);
    Colors.spect_trA = disp->alloc_color (xrm->get (".color.spect.trA", "#FF0000"),   Colors.black);
    Colors.spect_pkA = disp->alloc_color (xrm->get (".color.spect.pkA", "#FF8080"),   Colors.black);
    Colors.spect_trB = disp->alloc_color (xrm->get (".color.spect.trB", "#0000FF"),   Colors.black);
    Colors.spect_pkB = disp->alloc_color (xrm->get (".color.spect.pkB", "#8080FF"),   Colors.black);
    Colors.spect_trX = disp->alloc_color (xrm->get (".color.spect.trM", "darkgreen"), Colors.black);
    Colors.spect_trY = disp->alloc_color (xrm->get (".color.spect.trM", "magenta"),   Colors.black);
    Colors.butt_bg0  = disp->alloc_color (xrm->get (".color.butt.bg0",  "#e0d8d0"),   Colors.white);
    Colors.butt_bg1  = disp->alloc_color (xrm->get (".color.butt.bg1",  "yellow"),    Colors.white);
    Colors.butt_bgA  = Colors.spect_trA;
    Colors.butt_bgB  = Colors.spect_trB;
    Colors.butt_bgX  = Colors.spect_trX;
    Colors.butt_bgY  = Colors.spect_trY;

    XftColors.white    = disp->alloc_xftcolor ("white", 0);
    XftColors.black    = disp->alloc_xftcolor ("black", 0);
    XftColors.main_fg  = disp->alloc_xftcolor (xrm->get (".color.main.fg",   "black"), XftColors.black);
    XftColors.text_fg  = disp->alloc_xftcolor (xrm->get (".color.text.fg",   "black"), XftColors.black);
    XftColors.spect_fg = disp->alloc_xftcolor (xrm->get (".color.spect.fg",  "blue"),  XftColors.black);
    XftColors.spect_sc = disp->alloc_xftcolor (xrm->get (".color.spect.sc",  "black"), XftColors.black);
    XftColors.butt_fg0 = disp->alloc_xftcolor (xrm->get (".color.butt.fg0",  "black"), XftColors.black);
    XftColors.butt_fg1 = disp->alloc_xftcolor (xrm->get (".color.butt.fg1",  "black"), XftColors.black);
    XftColors.butt_fgA = disp->alloc_xftcolor (xrm->get (".color.butt.fgA",  "white"), XftColors.white);
    XftColors.butt_fgB = disp->alloc_xftcolor (xrm->get (".color.butt.fgB",  "white"), XftColors.white);
    XftColors.butt_fgX = disp->alloc_xftcolor (xrm->get (".color.butt.fgM",  "white"), XftColors.white);
    XftColors.butt_fgY = disp->alloc_xftcolor (xrm->get (".color.butt.fgM",  "white"), XftColors.white);
 
    XftFonts.about1 = disp->alloc_xftfont (xrm->get (".font.about1", "times:pixelsize=24"));
    XftFonts.about2 = disp->alloc_xftfont (xrm->get (".font.about2", "times:pixelsize=14"));
    XftFonts.button = disp->alloc_xftfont (xrm->get (".font.button", "luxi:pixelsize=12"));
    XftFonts.labels = disp->alloc_xftfont (xrm->get (".font.labels", "luxi:pixelsize=11"));
    XftFonts.scales = disp->alloc_xftfont (xrm->get (".font.scales", "luxi:pixelsize=9"));

    Bst0.font = XftFonts.button;
    Bst0.color.bg [0] = Colors.butt_bg0;
    Bst0.color.fg [0] = XftColors.butt_fg0;
    Bst0.color.bg [1] = Colors.butt_bg1;
    Bst0.color.fg [1] = XftColors.butt_fg1;
    Bst0.color.shadow.bgnd = Colors.main_bg;
    Bst0.color.shadow.lite = Colors.main_ls;
    Bst0.color.shadow.dark = Colors.main_ds;
    Bst0.size.x = 17;
    Bst0.size.y = 17;
    Bst0.type = X_button_style::RAISED;

    BstA = Bst0;
    BstA.color.bg [1] = Colors.butt_bgA;
    BstA.color.fg [1] = XftColors.butt_fgA;

    BstB = Bst0;
    BstB.color.bg [1] = Colors.butt_bgB;
    BstB.color.fg [1] = XftColors.butt_fgB;

    BstX = Bst0;
    BstX.color.bg [1] = Colors.butt_bgX;
    BstX.color.fg [1] = XftColors.butt_fgX;

    BstY = Bst0;
    BstY.color.bg [1] = Colors.butt_bgY;
    BstY.color.fg [1] = XftColors.butt_fgY;

    Tst0.font = XftFonts.labels;
    Tst0.color.normal.bgnd = Colors.white;
    Tst0.color.normal.text = XftColors.text_fg;
    Tst0.color.shadow.lite = Colors.main_bg;
    Tst0.color.shadow.dark = Colors.main_bg;
    Tst0.color.shadow.bgnd = Colors.main_bg;

    Tst1.font = XftFonts.labels;
    Tst1.color.normal.bgnd = Colors.main_bg;
    Tst1.color.normal.text = XftColors.main_fg;
    Tst1.color.shadow.lite = Colors.main_bg;
    Tst1.color.shadow.dark = Colors.main_bg;
    Tst1.color.shadow.bgnd = Colors.main_bg;
}

